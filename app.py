from flask import Flask, request
import time
import redis

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)


def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

@app.route('/', methods=['POST'])
def record():
    request.get_data()
    data = request.data

    cache.set(time.time(), data)
    return data

@app.route('/', methods=['GET'])
def getdata():
    return (str(cache.keys()))

if __name__ == "__main__":
    app.run(host='0.0.0.0')

#le message de paix suivant

	

